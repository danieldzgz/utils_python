#!/usr/bin/python3

# from https://gitlab.com/vtilloy/utils_python
# Purpose of the script: PCA (Principal Component Analysis) script to generate individuals factor map, variables factor map and parallel_coordinates plots

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from mpl_toolkits import mplot3d
from adjustText import adjust_text
from pandas.plotting import parallel_coordinates

# load data
data = pd.read_csv('data.txt', sep='\t', index_col=0)

# center and scale the data
data_scaled = (data - data.mean()) / data.std()

# perform PCA
pca = PCA()
principalComponents = pca.fit_transform(data_scaled)
loadings = pca.components_.T * np.sqrt(pca.explained_variance_)

# create scatter plot with thin black cross
fig, ax = plt.subplots(figsize=(10, 10))
ax.scatter(principalComponents[:,0], principalComponents[:,1], marker='+', color='black', alpha=0.8)

# annotate the plot with individual names
for i, txt in enumerate(data.index):
    # first 5 individuals will be colored in red, rest in blue
    if i < 5:
        ax.annotate(txt, (principalComponents[i,0], principalComponents[i,1]), color='red')
    else:
        ax.annotate(txt, (principalComponents[i,0], principalComponents[i,1]), color='blue')

plt.xlabel('PC1 ({:.2f}% of variance)'.format(pca.explained_variance_ratio_[0]*100))
plt.ylabel('PC2 ({:.2f}% of variance)'.format(pca.explained_variance_ratio_[1]*100))
plt.grid()

# add legend
red_patch = plt.scatter([],[], color='red', label='symptomatic samples')
blue_patch = plt.scatter([],[], color='blue', label='asymptomatic samples')
plt.legend(handles=[red_patch, blue_patch], loc='upper center')

plt.savefig('pca_plot.png')

# create plot with variables as arrows
fig, ax = plt.subplots(figsize=(10, 10))

# plot arrows representing variables
for i, v in enumerate(loadings.T):
    ax.arrow(0, 0, v[0], v[1], head_width=0.02, head_length=0.02)
    plt.text(v[0]* 1.2, v[1] * 1.2, data.columns[i], color='black', ha='center', va='center', fontsize=10)
plt.xlim(-1,1)
plt.ylim(-1,1)
plt.xlabel('PC1 ({:.2f}% of variance)'.format(pca.explained_variance_ratio_[0]*100))
plt.ylabel('PC2 ({:.2f}% of variance)'.format(pca.explained_variance_ratio_[1]*100))
plt.grid()
plt.savefig('pca_variables.png')

# define colors for the first 5 and remaining samples
colors = np.zeros((data.shape[0], 4))
colors[:5, :] = plt.get_cmap('Reds')(np.linspace(0.2, 1, 5))
colors[5:, :] = plt.get_cmap('Blues')(np.linspace(0.2, 1, data.shape[0] - 5))

# create parallel coordinates plot with custom colors for individuals
fig = plt.figure(figsize=(10, 10))
parallel_coordinates(data_scaled.reset_index(), 'index', color=colors)
plt.xticks(rotation=90)
plt.savefig('parallel_coordinates.png')

# create dataframe with coefficients
coefficients_df = pd.DataFrame(pca.components_, columns=data_scaled.columns)
coefficients_df.to_csv('coefficients.txt', index=False, sep='\t')