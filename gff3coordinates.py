#!/usr/bin/env python3

# from https://gitlab.com/vtilloy/utils_python
# Purpose of the script: Get coordinate from interest_sequence inside genome_sequence

from Bio.Seq import Seq
from Bio import pairwise2

interest_sequence = "GGGATGATGG"
genome_sequence = "ATGGGGATGATGGAAAAACCC"

# Find exact matches between interest_sequence and genome_sequence
matches = [i for i in range(len(genome_sequence)) if genome_sequence.startswith(interest_sequence, i)]

# Define GFF3 feature type and source
feature_type = "repeat_region"
source = "Genbank"

# Write GFF3 entries to a file
output_file = "interest_coordinates.gff3"
with open(output_file, "w") as f:
    for match_start in matches:
        match_end = match_start + len(interest_sequence)
        orientation = "+" if match_start < match_end else "-"
        gff3_entry = f"{genome_sequence}\t{source}\t{feature_type}\t{match_start + 1}\t{match_end}\t.\t{orientation}\t.\tID=id-xxx:{match_start + 1}..{match_end};Note=xxx;"
        f.write(gff3_entry + "\n")

print(f"Saved GFF3 coordinates to {output_file}")