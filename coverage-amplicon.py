#!/usr/bin/python3

# from https://gitlab.com/vtilloy/utils_python
# Purpose of the script: Provide coverage for each amplicon from sam files

import sys

def parse_sam(sam_file):
    alignments = []
    with open(sam_file, 'r') as sam:
        for line in sam:
            if not line.startswith('@'):
                fields = line.split('\t')
                chromosome = fields[2]
                position = int(fields[3])
                cigar = fields[5]
                alignments.append((chromosome, position, cigar))
    return alignments

def calculate_coverage(alignments, amplicons):
    amplicon_coverage = {}
    for amplicon in amplicons:
        chrom, left_primer, right_primer = amplicon
        coverage = 0
        for alignment in alignments:
            chrom_sam, pos_sam, cigar = alignment
            if chrom == chrom_sam and left_primer <= pos_sam <= right_primer:
                coverage += 1
        amplicon_coverage[amplicon] = coverage
    return amplicon_coverage

def save_amplicon_coverage_results(amplicon_coverage, output_file):
    with open(output_file, 'w') as result_file:
        for amplicon, coverage in amplicon_coverage.items():
            result_file.write(f'Amplicon {amplicon}: Coverage = {coverage}\n')

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python3 coverage-amplicon.py <input_sam_file> <output_txt_file>")
        sys.exit(1)

    input_sam_file = sys.argv[1]
    output_txt_file = sys.argv[2]

    amplicons = [
    ('MN908947.3', 25, 431),
    ('MN908947.3', 324, 727),
    ('MN908947.3', 644, 1044),
    ('MN908947.3', 944, 1362),
    ('MN908947.3', 1245, 1650),
    ('MN908947.3', 1540, 1948),
    ('MN908947.3', 1851, 2250),
    ('MN908947.3', 2154, 2571),
    ('MN908947.3', 2483, 2885),
    ('MN908947.3', 2826, 3210),
    ('MN908947.3', 3078, 3492),
    ('MN908947.3', 3390, 3794),
    ('MN908947.3', 3683, 4093),
    ('MN908947.3', 3992, 4409),
    ('MN908947.3', 4312, 4710),
    ('MN908947.3', 4620, 5017),
    ('MN908947.3', 4923, 5331),
    ('MN908947.3', 5230, 5643),
    ('MN908947.3', 5561, 5957),
    ('MN908947.3', 5867, 6272),
    ('MN908947.3', 6184, 6582),
    ('MN908947.3', 6478, 6885),
    ('MN908947.3', 6747, 7148),
    ('MN908947.3', 7057, 7467),
    ('MN908947.3', 7381, 7770),
    ('MN908947.3', 7672, 8092),
    ('MN908947.3', 7997, 8395),
    ('MN908947.3', 8304, 8714),
    ('MN908947.3', 8596, 9013),
    ('MN908947.3', 8919, 9329),
    ('MN908947.3', 9168, 9564),
    ('MN908947.3', 9470, 9866),
    ('MN908947.3', 9782, 10176),
    ('MN908947.3', 10076, 10491),
    ('MN908947.3', 10393, 10810),
    ('MN908947.3', 10713, 11116),
    ('MN908947.3', 11000, 11414),
    ('MN908947.3', 11305, 11720),
    ('MN908947.3', 11624, 12033),
    ('MN908947.3', 11937, 12339),
    ('MN908947.3', 12234, 12643),
    ('MN908947.3', 12519, 12920),
    ('MN908947.3', 12831, 13240),
    ('MN908947.3', 13124, 13528),
    ('MN908947.3', 13463, 13859),
    ('MN908947.3', 13752, 14144),
    ('MN908947.3', 14045, 14457),
    ('MN908947.3', 14338, 14743),
    ('MN908947.3', 14647, 15050),
    ('MN908947.3', 14953, 15358),
    ('MN908947.3', 15214, 15619),
    ('MN908947.3', 15535, 15941),
    ('MN908947.3', 15855, 16260),
    ('MN908947.3', 16112, 16508),
    ('MN908947.3', 16386, 16796),
    ('MN908947.3', 16692, 17105),
    ('MN908947.3', 16986, 17405),
    ('MN908947.3', 17323, 17711),
    ('MN908947.3', 17615, 18022),
    ('MN908947.3', 17911, 18328),
    ('MN908947.3', 18244, 18652),
    ('MN908947.3', 18550, 18961),
    ('MN908947.3', 18869, 19277),
    ('MN908947.3', 19183, 19586),
    ('MN908947.3', 19485, 19901),
    ('MN908947.3', 19810, 20216),
    ('MN908947.3', 20090, 20497),
    ('MN908947.3', 20377, 20792),
    ('MN908947.3', 20677, 21080),
    ('MN908947.3', 20988, 21387),
    ('MN908947.3', 21294, 21700),
    ('MN908947.3', 21532, 21933),
    ('MN908947.3', 21865, 22274),
    ('MN908947.3', 22091, 22503),
    ('MN908947.3', 22402, 22805),
    ('MN908947.3', 22648, 23057),
    ('MN908947.3', 22944, 23351),
    ('MN908947.3', 23219, 23635),
    ('MN908947.3', 23553, 23955),
    ('MN908947.3', 23853, 24258),
    ('MN908947.3', 24171, 24567),
    ('MN908947.3', 24426, 24836),
    ('MN908947.3', 24750, 25150),
    ('MN908947.3', 25051, 25461),
    ('MN908947.3', 25331, 25740),
    ('MN908947.3', 25645, 26050),
    ('MN908947.3', 25951, 26360),
    ('MN908947.3', 26255, 26661),
    ('MN908947.3', 26564, 26979),
    ('MN908947.3', 26873, 27283),
    ('MN908947.3', 27152, 27560),
    ('MN908947.3', 27447, 27855),
    ('MN908947.3', 27700, 28104),
    ('MN908947.3', 27996, 28416),
    ('MN908947.3', 28190, 28598),
    ('MN908947.3', 28512, 28914),
    ('MN908947.3', 28827, 29227),
    ('MN908947.3', 29136, 29534),
    ]

    alignments = parse_sam(input_sam_file)
    amplicon_coverage = calculate_coverage(alignments, amplicons)

    save_amplicon_coverage_results(amplicon_coverage, output_txt_file)