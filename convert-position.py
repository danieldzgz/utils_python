#!/usr/bin/env python3

# from https://gitlab.com/vtilloy/utils_python
# Purpose of the script: Convert VCF positions based on gene coordinates and strand information instead of whole genome coordinates

import argparse

parser = argparse.ArgumentParser(description='Convert VCF positions based on gene coordinates')
parser.add_argument('bed_file', type=str, help='Path to BED file with gene coordinates')
parser.add_argument('vcf_file', type=str, help='Path to VCF file with mutations')
parser.add_argument('output_file', type=str, help='Path to output VCF file')
args = parser.parse_args()

# Load gene coordinates and strand information from BED file
with open(args.bed_file) as f:
    gene_line = f.readline().strip().split()
    chrom, start, end, strand = gene_line[0], int(gene_line[1]), int(gene_line[2]), gene_line[5]

# Convert VCF positions based on gene coordinates and strand information, then write to output file
with open(args.vcf_file) as f, open(args.output_file, 'w') as out:
    for line in f:
        if line.startswith('#'):
            out.write(line)
        else:
            fields = line.strip().split()
            pos = int(fields[1])
            if strand == '+':
                new_pos = pos - start
            else:
                new_pos = end - pos
            fields[1] = str(new_pos)
            out.write('\t'.join(fields) + '\n')