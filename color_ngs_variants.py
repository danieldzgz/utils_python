#!/usr/bin/env python3

# from https://gitlab.com/vtilloy/utils_python
# Purpose of the script: Add color to NGS variants csv file (from aspicov/aspicmv/microbiomic) according to allelic frequency and presence into mutations.txt file

import sys
import pandas as pd
import openpyxl
import json

with open('mutations.txt') as file:
    mutation_list = json.load(file)

df = pd.DataFrame()

try:
    df = pd.read_csv('input.csv', sep=',')
except pd.errors.ParserError as e:
    print(f"Error reading CSV file: {e}")
    raise

if df.empty:
    headers = ['reference', 'genome_position', 'REF ALT', 'quality', 'depth', 'allelic_frequency', 'gene', 'amino_acid_mutation']
    df = pd.DataFrame(columns=headers)

df = df.fillna(0, limit=50)

result_file = 'final_color_output.xlsx'

legend_data = {
    'green': {
        'Description': "coding change <= 2% (non-silent and allelic frequency <= 0.002)",
        'Color': 'C1FFC1'
    },
    'yellow': {
        'Description': "coding change 2 to 20% (non-silent and allelic frequency between 0.002 and 0.02): not detected by Sanger sequencing",
        'Color': 'FFFFE0'
    },
    'orange': {
        'Description': "coding change 20% to 100% (if non-silent and allelic frequency superior to 0.02): detected by Sanger and Proton NGS",
        'Color': 'FFDAB9'
    },
    'red': {
        'Description': "coding change conferring antiviral resistance but not detected by Sanger (in BDD-v11 list and with allelic frequency below 0.02)",
        'Color': 'FFB6C1'
    },
    'other red': {
        'Description': "coding change conferring antiviral resistance and detected by Sanger (in BDD-v11 list and with allelic frequency above 0.02)",
        'Color': 'FF6347'
    },
    'blue': {
        'Description': "deletion",
        'Color': 'ADD8E6'
    },
    'other blue': {
        'Description': "codon Stop",
        'Color': '778899'
    }
}


with pd.ExcelWriter(result_file, engine='openpyxl') as writer:
    workbook = writer.book

    legend_df = pd.DataFrame.from_dict(legend_data, orient='index', columns=['Description', 'Color'])
    legend_sheet = workbook.create_sheet(title='legend')
    for idx, row in enumerate(legend_df.iterrows(), start=1):
        color_code = row[1]['Color']
        description = row[1]['Description']

        legend_sheet.cell(row=idx, column=1).value = description
        legend_sheet.cell(row=idx, column=1).fill = openpyxl.styles.PatternFill(start_color=color_code,
                                                                                  end_color=color_code,
                                                                                  fill_type='solid')

    worksheet = workbook.create_sheet(title='results')

    for col_idx, value in enumerate(df.columns, start=1):
        cell = worksheet.cell(row=1, column=col_idx)
        cell.value = value

    for idx, row in enumerate(df.iterrows(), start=2):
        mutation = str(row[1]['amino_acid_mutation'])
        gene = row[1]['gene']
        frequency_str = str(row[1]['allelic_frequency'])
        try:
            frequency = float(frequency_str)
        except ValueError:
            frequency = 0

        row_color = None
        if mutation.startswith('p.') and mutation[2:5] == mutation.split('.')[-1][-3:]:
            row_color = 'white'
        elif 'Stop' in mutation:
            row_color = 'other blue'
        elif 'del' in mutation:
            row_color = 'blue'
        elif any((gene == m_entry[1] and mutation in m_entry[0]) for m_entry in mutation_list):
            row_color = 'red' if frequency <= 0.02 else 'other red'
        else:
            if frequency <= 0.002:
                row_color = 'green'
            elif 0.002 < frequency <= 0.02:
                row_color = 'yellow'
            elif 0.02 < frequency <= 1:
                row_color = 'orange'

        for col_idx, value in enumerate(row[1].values, start=1):
            cell = worksheet.cell(row=idx, column=col_idx)
            cell.value = value

            if row_color:
                color_mapping = {
                    'white': 'FFFFFF',
                    'green': 'C1FFC1',
                    'yellow': 'FFFFE0',
                    'orange': 'FFDAB9',
                    'red': 'FFB6C1',
                    'other red': 'FF6347',
                    'blue': 'ADD8E6',
                    'other blue': '778899'
                }
                cell.fill = openpyxl.styles.PatternFill(start_color=color_mapping[row_color],
                                                          end_color=color_mapping[row_color], fill_type='solid')

    worksheet.column_dimensions['A'].width = 20
    worksheet.delete_cols(len(row[1].values) + 1)